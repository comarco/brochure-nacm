<?php

/**
 * Implementation of hook_context_default_contexts().
 */
function kt_acsf_birth_stories_context_default_contexts() {
  $export = array();

  $context = new stdClass;
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'birth_stories_context';
  $context->description = 'The blocks that are on the frontpage';
  $context->tag = '';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'birth_story' => 'birth_story',
      ),
      'options' => array(
        'node_form' => '0',
      ),
    ),
    'path' => array(
      'values' => array(
        'birth-stories' => 'birth-stories',
        'birth-stories/*' => 'birth-stories/*',
        'birth-stories-archive' => 'birth-stories-archive',
        'birth-stories-archive/*' => 'birth-stories-archive/*',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-births-block_1' => array(
          'module' => 'views',
          'delta' => 'births-block_1',
          'region' => 'right',
          'weight' => 0,
        ),
        'views-01523068f9da1b02e4f206defe203990' => array(
          'module' => 'views',
          'delta' => '01523068f9da1b02e4f206defe203990',
          'region' => 'right',
          'weight' => 1,
        ),
      ),
    ),
    'debug' => array(
      'debug' => 1,
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('The blocks that are on the frontpage');
  $export['birth_stories_context'] = $context;

  return $export;
}
