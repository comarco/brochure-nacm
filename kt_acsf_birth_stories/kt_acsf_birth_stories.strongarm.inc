<?php

/**
 * Implementation of hook_strongarm().
 */
function kt_acsf_birth_stories_strongarm() {
  $export = array();

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_anonymous_birth_story';
  $strongarm->value = 0;
  $export['comment_anonymous_birth_story'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_birth_story';
  $strongarm->value = '0';
  $export['comment_birth_story'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_controls_birth_story';
  $strongarm->value = '3';
  $export['comment_controls_birth_story'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_mode_birth_story';
  $strongarm->value = '4';
  $export['comment_default_mode_birth_story'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_order_birth_story';
  $strongarm->value = '1';
  $export['comment_default_order_birth_story'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_default_per_page_birth_story';
  $strongarm->value = '50';
  $export['comment_default_per_page_birth_story'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_form_location_birth_story';
  $strongarm->value = '0';
  $export['comment_form_location_birth_story'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_preview_birth_story';
  $strongarm->value = '1';
  $export['comment_preview_birth_story'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'comment_subject_field_birth_story';
  $strongarm->value = '1';
  $export['comment_subject_field_birth_story'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'content_extra_weights_birth_story';
  $strongarm->value = array(
    'title' => '-28',
    'body_field' => '-25',
    'revision_information' => '-12',
    'author' => '-13',
    'options' => '-11',
    'menu' => '-14',
  );
  $export['content_extra_weights_birth_story'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'date:birth_story:full:field_bs_publish_on_fromto';
  $strongarm->value = 'both';
  $export['date:birth_story:full:field_bs_publish_on_fromto'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'date:birth_story:full:field_bs_publish_on_multiple_from';
  $strongarm->value = '';
  $export['date:birth_story:full:field_bs_publish_on_multiple_from'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'date:birth_story:full:field_bs_publish_on_multiple_number';
  $strongarm->value = '';
  $export['date:birth_story:full:field_bs_publish_on_multiple_number'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'date:birth_story:full:field_bs_publish_on_multiple_to';
  $strongarm->value = '';
  $export['date:birth_story:full:field_bs_publish_on_multiple_to'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'date:birth_story:full:field_bs_publish_on_show_repeat_rule';
  $strongarm->value = 'show';
  $export['date:birth_story:full:field_bs_publish_on_show_repeat_rule'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'date:birth_story:teaser:field_bs_publish_on_fromto';
  $strongarm->value = 'both';
  $export['date:birth_story:teaser:field_bs_publish_on_fromto'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'date:birth_story:teaser:field_bs_publish_on_multiple_from';
  $strongarm->value = '';
  $export['date:birth_story:teaser:field_bs_publish_on_multiple_from'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'date:birth_story:teaser:field_bs_publish_on_multiple_number';
  $strongarm->value = '';
  $export['date:birth_story:teaser:field_bs_publish_on_multiple_number'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'date:birth_story:teaser:field_bs_publish_on_multiple_to';
  $strongarm->value = '';
  $export['date:birth_story:teaser:field_bs_publish_on_multiple_to'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'date:birth_story:teaser:field_bs_publish_on_show_repeat_rule';
  $strongarm->value = 'show';
  $export['date:birth_story:teaser:field_bs_publish_on_show_repeat_rule'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'enable_revisions_page_birth_story';
  $strongarm->value = 1;
  $export['enable_revisions_page_birth_story'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'ffp_birth_story_field_bs_audios';
  $strongarm->value = 0;
  $export['ffp_birth_story_field_bs_audios'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'ffp_birth_story_field_bs_files';
  $strongarm->value = 1;
  $export['ffp_birth_story_field_bs_files'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'ffp_birth_story_field_bs_img_gal';
  $strongarm->value = 1;
  $export['ffp_birth_story_field_bs_img_gal'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'ffp_birth_story_field_bs_teaser_img';
  $strongarm->value = 0;
  $export['ffp_birth_story_field_bs_teaser_img'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'ffp_birth_story_field_bs_videos';
  $strongarm->value = 0;
  $export['ffp_birth_story_field_bs_videos'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'language_content_type_birth_story';
  $strongarm->value = '0';
  $export['language_content_type_birth_story'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'node_options_birth_story';
  $strongarm->value = array(
    0 => 'status',
    1 => 'promote',
  );
  $export['node_options_birth_story'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'show_diff_inline_birth_story';
  $strongarm->value = 0;
  $export['show_diff_inline_birth_story'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'show_preview_changes_birth_story';
  $strongarm->value = 1;
  $export['show_preview_changes_birth_story'] = $strongarm;

  $strongarm = new stdClass;
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'upload_birth_story';
  $strongarm->value = '1';
  $export['upload_birth_story'] = $strongarm;

  return $export;
}
