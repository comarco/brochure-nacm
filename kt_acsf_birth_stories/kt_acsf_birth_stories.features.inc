<?php

/**
 * Implementation of hook_ctools_plugin_api().
 */
function kt_acsf_birth_stories_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "context" && $api == "context") {
    return array("version" => 3);
  }
  elseif ($module == "strongarm" && $api == "strongarm") {
    return array("version" => 1);
  }
}

/**
 * Implementation of hook_node_info().
 */
function kt_acsf_birth_stories_node_info() {
  $items = array(
    'birth_story' => array(
      'name' => t('Birth Story'),
      'module' => 'features',
      'description' => t('A <em>Birth story</em>, similar in form to a <em>page</em>, will be published in the Birth stories section.

Birth Stories are PRIVATE by default. To make it public, go to the Birth story page and click on the "Access Control" link (top right of the content). You can set the content access "View" to "Anonymous".'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'has_body' => '1',
      'body_label' => t('Body'),
      'min_word_count' => '0',
      'help' => '',
    ),
  );
  return $items;
}

/**
 * Implementation of hook_views_api().
 */
function kt_acsf_birth_stories_views_api() {
  return array(
    'api' => '2',
  );
}
