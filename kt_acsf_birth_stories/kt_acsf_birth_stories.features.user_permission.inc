<?php

/**
 * Implementation of hook_user_default_permissions().
 */
function kt_acsf_birth_stories_user_default_permissions() {
  $permissions = array();

  // Exported permission: create birth_story content
  $permissions['create birth_story content'] = array(
    'name' => 'create birth_story content',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'editor',
    ),
  );

  // Exported permission: delete any birth_story content
  $permissions['delete any birth_story content'] = array(
    'name' => 'delete any birth_story content',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'editor',
    ),
  );

  // Exported permission: delete own birth_story content
  $permissions['delete own birth_story content'] = array(
    'name' => 'delete own birth_story content',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'authenticated user',
      '2' => 'editor',
    ),
  );

  // Exported permission: edit any birth_story content
  $permissions['edit any birth_story content'] = array(
    'name' => 'edit any birth_story content',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'editor',
    ),
  );

  // Exported permission: edit own birth_story content
  $permissions['edit own birth_story content'] = array(
    'name' => 'edit own birth_story content',
    'roles' => array(
      '0' => 'administrator',
      '1' => 'authenticated user',
      '2' => 'editor',
    ),
  );

  return $permissions;
}
