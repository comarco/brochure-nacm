<?php

/**
 * Implementation of hook_menu_default_menu_links().
 */
function kt_acsf_news_events_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: primary-links:birth-stories
  $menu_links['primary-links:birth-stories'] = array(
    'menu_name' => 'primary-links',
    'link_path' => 'birth-stories',
    'router_path' => 'birth-stories',
    'link_title' => 'Birth Stories',
    'options' => array(
      'attributes' => array(
        'title' => 'Birth Stories',
      ),
    ),
    'module' => 'system',
    'hidden' => '0',
    'external' => '0',
    'has_children' => '0',
    'expanded' => '0',
    'weight' => '-46',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Birth Stories');


  return $menu_links;
}
